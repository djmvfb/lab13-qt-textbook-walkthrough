#include <QApplication>
#include <QTextEdit>


int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    QTextEdit text_editor;
    text_editor.setWindowTitle("Not Vim");
    text_editor.show();

    return app.exec();
}

