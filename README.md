First, you'll want to create your `main.cpp` file.
For convenience, there already exists one in the subdirectories of this 
directory.
Go ahead and navigate into one of those directories.
For instance,

    $ cd 00-init

Next, create the `.pro` file.
The following command will create a `.pro` file named according to the name
of the current working directory.

    $ qmake -project

Don't forget to enable widgets in the `.pro` file!

    $ echo "QT += widgets" >> DIRECTORY_NAME.pro

Note that `DIRECTORY_NAME` needs to be changed to the actual name of the
`.pro` file.

After creating an appropriate `.pro` file, create a `Makefile` like so:

    $ qmake

Now, you are ready to compile your program.

    $ make

Assuming there were no errors, this ought to create an executable.
Running the executable is left as an exercise to the reader.

